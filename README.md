# User Compatibility Pack for Confluence

With Confluence 5.2, we are introducing the ability to change usernames. To make that possible, users now have a new, unique, permanent key as well as the already-existing, unique, changeable username.

Confluence 5.2 introduces a new Interface to represent users, called `ConfluenceUser`.

## What is the compatibility pack?

Atlassian provides a compatibility library that your plugin can include so that it can interact with either Confluence 3.5 or Confluence 5.2 and later, in a safe manner.

## When do I need it?

If you want to be compatible with Confluence 3.5 and later, you cannot use `ConfluenceUser` class directly, as it was introduced in Confluence 5.2.

## How to use it?

Add a maven dependency by adding following to your `pom.xml`:

    <dependency>
        <groupId>com.atlassian.usercompatibility</groupId>
        <artifactId>usercompatibility-confluence</artifactId>
        <version>2.1.4</version>
    </dependency>
